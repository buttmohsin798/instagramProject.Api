<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ArticleHashtagTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles_hashtags', function (Blueprint $table) {
           $table->bigIncrements('id');
           $table->bigInteger('articles_id')->unsigned()->index();
           $table->bigInteger('hashtags_id')->unsigned()->index();
           $table->timestamps();

           $table->foreign('articles_id')->references('id')->on('articles')->onDelete('cascade');
           $table->foreign('hashtags_id')->references('id')->on('hashtags')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('article-hashtag');

    }

}
