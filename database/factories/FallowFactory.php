<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\fallow::class, function (Faker $faker) {
    return [
       'user_following'=>$faker->randomDigit(),
        'user_follower'=>$faker->randomDigit()
    ];
});
