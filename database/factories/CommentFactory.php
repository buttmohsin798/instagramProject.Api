<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;
use App\Comment;

$factory->define(Comment::class, function (Faker $faker) {
    return [

        'body'=>$faker->paragraph,
        'commentable_id'=>\App\User::query()->inRandomOrder()->take(1)->first()->id,
        'commentable_type'=>\App\User::class,
        'article_id'=>\App\Article::query()->inRandomOrder()->take(1)->first()->id,
    ];
});
