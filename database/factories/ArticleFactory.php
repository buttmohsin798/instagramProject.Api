<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Article::class, function (Faker $faker) {
    return [
        'user_id' => $faker->randomDigit(),
        'title' => $faker->sentence,
        'description' => $faker->paragraph,
        'image' => $faker->imageUrl,
        'like_article' => $faker->randomDigit()
    ];
});
