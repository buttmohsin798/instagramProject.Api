<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\hashtag::class, function (Faker $faker) {
    return [
        'hashtag'=>$faker->name,
        'count_used'=>$faker->randomDigit(1)
    ];
});
