<?php

use Illuminate\Database\Seeder;

class FallowSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\fallow::class,50)->create();
    }
}
