<?php

use Illuminate\Database\Seeder;

class hashtagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\hashtag::class,50)->create();
    }
}
