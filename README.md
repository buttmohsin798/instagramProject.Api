A social network similar to Instagram has been implemented with the following features.
* Ability to like posts
* Ability to search for posts based on the most likes
* Ability to search for posts saved by the user
* Ability to search for posts based on hashtags
* View three hashtags in the last 24 hours
* ...
