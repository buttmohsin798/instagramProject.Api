<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

class Hashtag extends Model
{
    const NAME_MIN_LENGTH = 5;
    protected $fillable = ['count_view', 'hashtag'];


    public function articles()
    {
        return $this->belongsToMany(Article::class);
    }


    public function searchPostsBasedHashtags($request)
    {

        $data = $request['search'];
        if (strlen($data) < self::NAME_MIN_LENGTH) {
            return response()->json([
                'message' => 'auth.searchPostsBasedHashtags'
            ]);
        }
       $hashtag = Hashtag::where('hashtag', 'like', "{$data}")->get('id');
       $article=Hashtag::find($hashtag[0]['id'])->articles()->orderby('title')->get();
        //dd(Arr::flatten($article));
         if (Arr::flatten($article)==[]){
             return response()->json([
                 'error' => 'No posts found'
             ]);
         }else{
           // dd(Arr::flatten($article)[0]['title']);
             return response()->json([
                 'data' => Arr::flatten($article)[0]['title']
             ]);
         }
    }
    public function removeArticleSpecificHashtag($hashtag)
    {
//dd($hashtag->id);
        $article=Hashtag::find($hashtag->id)->articles()->orderby('title')->get();
       // dd(Arr::flatten($article));

        if (Arr::flatten($article)==[]) {
            return response()->json(['error' => 'There is no post for this hashtag']);
        }

            $articless = $article->modelKeys()[0];

            $x = Article::find($articless);
            $x->delete($articless);
            return response()->json(['message' => 'Post deleted']);
        }





}
