<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fallow extends Model
{
    protected $fillable=[
        'user_following','user_follower'
    ];

    public function Users()
    {
        return $this->belongsTo(User::class);
    }
}
