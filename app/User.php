<?php

namespace App;

use App\Http\Resources\userCollection;
use App\Services\Comment\Traits\Commentable;
use App\Services\User\ValidateUser;
use http\Env\Request;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;


class User extends Authenticatable implements JWTSubject
{
    use Notifiable, Commentable;

    const NAME_MIN_LENGTH = 5;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'image'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    /*  protected $hidden = [
          'password', 'remember_token',
      ];*/

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function fallowUser($request)
    {
        //dd($request);
        $validater = new ValidateUser();
        $error=$validater->validateFallowUser($request);
        if ($error) {
            return response()->json([
                'error' => $error

            ]);
        } else {
            Fallow::create([
                'user_following' => $request->user_following,
                'user_follower' => $request->user_follower
            ]);
            return response()->json([
                'data' => [
                    'message' => 'The selected user was followed.'
                ]
            ]);
        }
    }


    public static function editUser($request,$User)
    {
        $validateEditUser=new ValidateUser();
        $error= $validateEditUser->validateEditUser($request,$User);
       if ($error){
           return  response()->json(['error'=>$error]);
       }else{
           $user = User::find($User);
           $user->update($request->all());
           return  response()->json(['message'=>'Updated']);
       }


    }

    public function deleteUser($User)
    {
        $user=User::Find($User);
        if (!$user){
            return respons()->json(['error'=>'not user']);
        }else{
            User::delete($User);
            return response()->json(['message'=>'deleted']);
        }
    }

    public static function createUser($request)
    {
        $validateEditUser=new ValidateUser();
        $error=$validateEditUser->validateCreateUser($request);
        if ($error ){
            return  response()->json(['error'=>$error]);
        }else{
            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => $request->password,
                'image' => $request->image
            ]);
            return response()->json([
                'data' => [
                    'name' => $user->name,
                    'message' => 'Registration completed successfully.']
            ], 200);
        }

        }


    public static function viewAccountAndPostsAccount($User)
    {

        $user=User::with('articles')->Find($User);

        if (!$user){
            return response()->json([
                'message'=>'user Invalid'
            ]);
        }
        return response()->json([
            'data' =>  $user
        ], 200);

    }
    public static function searchUsername($request)
    {
        // dd($request);

        $data = $request->query('search');
        //dd($data);
        // dd(strlen($data));
        if (strlen($data) < self::NAME_MIN_LENGTH) {
            return response()->json([
                'message' => 'The name entered is short.'
            ]);
        }

        $user = User::where('name', 'like', "{$data}")->get();

        return response()->json([
            'data' => $user
        ]);
    }




    public static function DisplayCommentsPostedUser($id)
    {
        $user = User::with('comments')->Find($id);
        //dd($user->comments);
        if (!$user or $user == null) {
            return response()->json([
                'message' => 'Invalid Input'
            ]);
        } else {
            return response()->json([
                'data' => new userCollection($user)
            ]);
        }
    }

    public function articles()
    {
        return $this->hasMany(Article::class);
    }

    public function fallows()
    {
        return $this->hasMany(Fallow::class);
    }

    public function comment()
    {
        $this->belongsTo(Comment::class);
    }


    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
