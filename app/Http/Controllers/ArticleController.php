<?php

namespace App\Http\Controllers;

use App\Article;
use App\Comment;
use App\Hashtag;
use App\Http\Requests\ArticleRequest;
use App\Http\Resources\ArticleCollection;
use App\Http\Resources\ArticleResource;
use App\Http\Resources\commentCollection;
use App\Http\Resources\userCollection;
use App\Services\Articles\ValidateArticle;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use function GuzzleHttp\Promise\all;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $article;
    protected $request;

    public function __construct(Article $article)
    {
        $this->article = $article;

    }

    public function index(Request $request)
    {


        /*----------------------------View trending hashtags in the last 24 hours---------------*/
           //return $this->article->viewTrendingHashtagsLast24Hour($request);
    }

    /*----------------------------Ability to search for posts based on hashtags-------------*/
    public function searchPostsBasedHashtags(Hashtag $hashtag,Request $request)
    {
        //dd($request['search']);
        return $hashtag->searchPostsBasedHashtags($request);
    }

    /*----------------------------View trending hashtags in the last 24 hours---------------*/
    public function viewTrendingHashtagsLast24Hour(Article $article,Request $request)
    {

        return $article->viewTrendingHashtagsLast24Hour($request);
     }


    /*--------------------------------- CreateArticle----------------------*/
    public function CreateArticle(Article $article,Request $request)
    {
        return $article->CreateArticle($request);
    }
    /*--------------------------Show search based on the most likes----------------------*/
    public function showSearchBasedOnTheMostLikes(Article $article,Request $request)
    {

        return $article->showSearchBasedOnTheMostLikes($request);
    }
    /*-------------------------the show all Article-------------------------*/
    public function showAllArticle(Article $article,Request $request)
    {
        return $article->showAllArticle($request);
    }




    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*--------------------------------- CreateArticle----------------------*/

        //return $this->article->CreateArticle($request);
    }

    /*----------------------------------Create hashtags in the post-------------*/
    public function createHashtagPost(Article $article,Hashtag $hashtag)
    {

        return $article->createHashtagPost($article,$hashtag);
    }
    /*----------------------------------like article----------------------------*/
    public function likeArticle (User $user,Article $article)
    {

         return $article->likeArticle($user,$article);
    }


    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {


    }

    /*----------------------View comments on each post-------------------*/
    public function viewCommentsPost (Article $article)
    {
       // dd($Article);
        return $this->article->viewCommentsPost($article);  //Get post ID
    }

    /*----------------------View hashtags in a post----------------------------------*/
    public function viewHashtagsPost(Article $article)
    {
        if (!$article->id){
            return response()->json(['error'=>'No posts found']);
        } return $article->viewHashtagsPost($article); //Get post ID

    }
    /*---------------------View photos of one post---------------------------*/

    public function viewPhotosOnePost(Article $article)
    {

        return $article->viewPhotosOnePost($article);
    }
    /*----------------------- View photos of all posts-----------------------------------   */
    public function viewPhotosAllPosts(Article $article)
    {

        return $article->viewPhotosAllPosts($article);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        }
    /*---------- Edit the Article photo if it is null/inNull------------------------------*/
    public function editArticlePhoto(Article $article,Request $request)
    {

        return $article->editArticlePhoto($request,$article);
    }

    /*----------End Edit the Article's photo if it is null/inNull------------------------------*/


    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

/*----------------------Ability to delete posts based on specific hashtags------*/

        //return $this->article->removeArticleSpecificHashtag($id);
    }

    /*----------------------Ability to delete posts based on specific hashtags------*/
    public function removeArticleSpecificHashtag(Hashtag $hashtag)
    {
        //dd($hashtag);
        return $hashtag->removeArticleSpecificHashtag($hashtag);
    }
    /*--------------------remove article---------------------------------*/
    public function removeArticle(Article $article)
    {
        return $article->removeArticle($article);
    }

}
