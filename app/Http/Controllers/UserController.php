<?php

namespace App\Http\Controllers;


use App\Article;
use App\Comment;
use App\Fallow;
use App\Http\Requests\SearchRequest;
use App\Http\Requests\UserRequest;
use App\Http\Resources\ArticleResource;
use App\Http\Resources\UserAddCollection;
use App\Http\Resources\userCollection;
use App\Http\Resources\userResource;
use App\User;
use Illuminate\Http\Request;


class UserController extends Controller
{
    protected $user;
    protected $comment;
    public function __construct(User $user,Comment $comment)
    {
       $this->user=$user;
       $this->comment=$comment;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
    }
    /*------------------------search user to name user---------------------------*/
    public function searchUsername(User $user,Request $request)
    {
        //dd($request);
        return $user->searchUsername($request);
    }

    /*---------------------------------------------------------------------------------*/

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }
    /*-----------------------------create user-----------------*/
    public function createUser(User $user, Request $request)
    {

        return $user->createUser($request);
    }

    /*-------------- Edit user information-----------------*/
    public function editUser(User $user, Request $request)
    {
        if (!$user) {
            return response()->json(['error'=>'Invalid Input\n']);
        }
        return $user->editUser($request, $user);
    }


    /*----------------------------------Ability to like comments----------------------------*/
    public function likeComment(User $user,Comment $comment)
    {
       // dd($comment);
        return $comment->likeComment($user,$comment);
    }
    /*----------------------------------------Ability to leave comments with user ID and post input----------------------*/
    public function leaveCommentsWithInputUserIdAndPost(Comment $comment,Request $request)
    {
        dd($request);
        return $comment->leaveCommentsWithInputUserIdAndPost($request);
    }
    /*---------------------------Fallow User---------------------------------*/
    public function fallowUser(User $user,Request $request)
    {

        return $user->fallowUser($request);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {

        /*------------------------Display comments posted by the user--------------------*/
      //  return $this->user->DisplayCommentsPostedUser($id);



    }

    /*-----------------View the account and the posts(search) related to that account--------------*/
    public function viewAccountAndPostsAccount(User $user)
    {

        return $user->viewAccountAndPostsAccount($user);
          }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *with
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {

       /* $user=User::FindorFail($id);
        $user->update($request->all());*/

    }
    /*---------------------------delete user--------------------------*/
    public function deleteUser(User $user)
    {
      return $user->deleteUser($user);
    }





}
