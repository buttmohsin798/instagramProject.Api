<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class UserAddCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            /*'data'=>userResource::collection($this->collection),*/
            'name'=>$this->name,

            /*'articles' => UserAddResource::collection($this->collection)*/
        ];
}
}
