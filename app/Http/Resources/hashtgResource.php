<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class hashtgResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'hashtag'=>$this->hashtag
        ];
    }
}
