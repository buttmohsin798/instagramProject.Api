<?php

namespace App\Http\Resources;

use App\Article;
use App\User;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Arr;

class userCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    protected $count;

    public function toArray($request)
    {
        // $Article=Article::where('user_id',3)->first();
        //dd($this->collection);
        /*-----------------View the account and the posts(search)related to that account--------------*/

        return [

            'name' => $this->name,
            'email' => $this->email,


            'articles' => ArticleResource::collection($this->articles)
        ];


        /*------------------------Display comments posted by the user--------------------*/

           /* if ($this->comments){
                return ['name'=>$this->name,
                    'message'=>'No comments have been user.'

                ];
            }else{
                return [
                    'name'=>$this->name,
                    'comments' => ArticleResource::collection($this->comments)
                ];
            }*/









        /*search to name user*/
        /*  return ['data'=>userResource::collection($this->collection)];*/

    }

}
