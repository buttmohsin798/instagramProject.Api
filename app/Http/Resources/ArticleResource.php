<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ArticleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {


        /*-----------------View the account and the posts(search) related to that account--------------*/
       return [
            'title'=>$this->title,
            'description'=>$this->description,
            'image_url'=>$this->image,
            'user'=>$this->user->name
        ];


        /*------------------------Display comments posted by the user--------------------*/

        /*return [
          'comment'=>$this->body
        ];*/

    }
}
