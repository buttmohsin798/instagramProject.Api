<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Arr;

class ArticleCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //   dd(Arr::flatten($this));
        /*-----------------------Show search based on the most likes-----------*/
       return [

            'title' => $this->title,
            'description' =>$this->description,
            'image' =>$this->image,
            'like_article'=>$this->like_article

        ];
       /*return [
            'image' =>$this->image

        ];*/

    }
}
