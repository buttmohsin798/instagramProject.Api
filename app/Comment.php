<?php

namespace App;

use App\Services\User\ValidateUser;
use http\Env\Response;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{

    protected $fillable=[
      'commentable_id','article_id','body','like_comment','commentable_type'
    ];
    /*protected $comment;

    public function __construct(Comment $comment)
    {
        $this->comment=$comment;
    }*/


    public static function likeComment($user,$comment)
    {
        $validateLikeComment= new ValidateUser;
        $error=$validateLikeComment->validateLikeComment($user,$comment);
        if ($error){
            return response()->json([
                'error' => $error
            ]);
        } else                                                //Check the existence of users in the database
        {
            $number = 1;

            $comment = Comment::find($comment);                 //Any value in the field is added to it.
            $comment->Like_comment = $comment->like_comment + $number;
            $comment->save();
            return response()->json([
                'data' => 'Likes were registered'
            ]);
        }

    }

    public static function leaveCommentsWithInputUserIdAndPost($request)
    {
        //dd($request);

        $validateUser= new ValidateUser;
        $error=$validateUser->validateLeaveCommentsWithInputUserIdAndPost($request);
        if ($error){
            return response()->json([
                'message'=>$error
            ]);
        }
       // dd($request->article_id);
          Comment::create([
              'commentable_id'=> $request->user_id,
              'article_id'=>$request->article_id,
              'body'=>$request->comment,
              'like_comment'=>0,
              'commentable_type'=>'App\User'
          ]);
          return response()->json([
             'message'=>'Comment posted'
          ]);
    }

    public function users()
    {
        return $this->hasMany(User::class);
    }

    public function article()
    {
        return $this->belongsTo(Article::class);
    }
}
