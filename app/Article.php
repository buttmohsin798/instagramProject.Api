<?php

namespace App;

use App\Http\Requests\ArticleRequest;
use App\Http\Resources\ArticleCollection;
use App\Http\Resources\commentCollection;
use App\Http\Resources\hashtgCollection;
use App\Services\Article\ValidateArticle;
use App\Services\Comment\Traits\Commentable;
use http\Env\Response;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\Types\False_;
use phpDocumentor\Reflection\Types\Null_;

class Article extends Model
{
    const NAME_MIN_LENGTH = 5;
    protected $fillable = [
        'user_id', 'title', 'description', 'image', 'like_article'
    ];

    /*public $article;
     public function __construct(Article $article)
     {
         $this->article = $article;
     }*/
    public function hashtags()
    {
        return $this->belongsToMany(Hashtag::class);
    }


    public function viewHashtagsPost($Article)
    {

        $hashtags = $this->with('hashtags')->find($Article->id);  //Here we received the post and hashtags related to that post.
        //dd(count($hashtags['hashtags']));    //We used this index to get only its hashtags and counted on it to determine the total number of hashtags.
        if (!count($hashtags['hashtags'])) {

            return response()->json([
                    'message' => 'auth.viewHashtagsPost'
                ]
            );
        } else
            return response()->json(
                [

                    'data' => new hashtgCollection($hashtags)
                ]
            );
    }





    public function createHashtagPost($Article,$Hashtag)
    {
        $validateArticle = new ValidateArticle();


        $errorMsg = $validateArticle->isValidHashtagPost( $Article, $Hashtag);
//dd($errorMsg);
        if ($errorMsg) {
            return response()->json(['message' => $errorMsg], 201);
        } else {

            $number = 1;
            $Hashtag->count_used = $Hashtag->count_used + $number;
            $Hashtag->save();
            $Article->hashtags()->attach($Hashtag);
            return response()->json(['message' => 'auth.createHashtagPost'], 201);
        }

    }



    public static function removeArticle($article)
    {


        //dd($article);

        if (!$article) {
            return response()->json([
                'message' => 'No posts'
            ]);
        }

        $article->delete($article);
        return response()->json(['message' => 'Post deleted']);
    }

    public static function showAllArticle($request)
    {
        $articles = Article::all();
        return response()->json(
            ['data' => Arr::flatten($articles)]
        );
    }

    public static function editArticlePhoto($request,$article)
    {
        $validateArticle=resolve(ValidateArticle::class);
       $error=$validateArticle->validateEditArticlePhoto($request);
        if ($error) {
            return response()->json([
                'error' => $error
            ]);
        }
        $article->update($request->all());
        return response()->json(['message' => 'Photo uploaded / edited']);
    }

    public function CreateArticle($request)
    {
        $validateArticle = new ValidateArticle;
         $error=$validateArticle->validateCreateArticle($request);
        if ($error) {
            return response()->json(['errors' => $error]);
        }

        Article::create([
            'user_id' => $request->id,
            'title' => $request->title,
            'description' => $request->description,
            'image' => $request->image
        ]);
        return response()->json(['message' => 'created Article for you'], 201);

    }


    public static function viewPhotosAllPosts($Article)
    {
        $article = Article::find($Article);
        if (!$article) {
            return response()->json(
                ['message' => 'Input Invalid']
            );
        }
        $articles = Article::all('image');
        // dd($articles);
        return response()->json(
            ['data' => $articles]
        );
    }

    public function viewTrendingHashtagsLast24Hour($request)
    {
        $limit = $request['limit'];

        $hashtags = Hashtag::all('count_used', 'hashtag');
        $hashtagsSort = $hashtags->sortByDesc('count_used');


        return response()->json([
            'data' => $hashtagsSort->skip(0)->take($limit)
        ]);

    }

    public static function showSearchBasedOnTheMostLikes($request)
    {
        if (!$request->title) {
            return response()->json([
                'message' => 'Invalid Input'
            ]);
        }
        $data = $request->title;
        //dd($data);
        $article = Article::where('title', 'like', "%{$data}%")->get();
        // dd($article);
        if (!$article) {
            return response()->json([
                'data' => 'Nothing found'
            ]);
        } else {
            $article = $article->sortByDesc('like_article');

            return response()->json([
                'data' => ArticleCollection::collection($article)

            ]);
        }

    }


    public function likeArticle($User,$Article)
    {
        //dd($request);
        $validateLikeArticle = new ValidateArticle();
        // dd($validateLikeArticle->ValidateLikeArticle($request));
        $error=$validateLikeArticle->ValidateLikeArticle($User,$Article);
        if ($error) {
            return response()->json([
                'error' => $error
            ]);
        }
        //dd('hello');
        $article = Article::find($Article);

        // dd($article);
        $number = 1;
        $article->Like_article = $article->like_article + $number;
        $article->save();
        return response()->json([
            'data' => 'Likes were registered'
        ]);
    }


    public static function viewCommentsPost($article)
    {

        $comments = Article::with('comments')->find($article->id);

         //dd($comments);
        if (!$comments) {
            return response()->json([
                    'message' => 'There are no comments for this Article.'
                ]
            );
        } elseif ($article->id) {
            return response()->json(
                [
                    'title' => $comments->title,
                    'data' => new commentCollection($comments)
                ]
            );
        }

    }

    public static function viewPhotosOnePost($articles)
    {
        //dd($id);

        if (!$articles) {
            return response()->json([
                'message' => 'Invalid Input'
            ]);
        }
        // dd($articles);
        return response()->json(
            ['data' => $articles->image ]);

    }

    public function User()
    {
        return $this->belongsTo(User::class);
    }

    public function comments()
    {
        // dd('hello');
        return $this->hasMany(Comment::class);
    }

    public function incrementLike($number = 1)
    {
        $like_article = 0;
        return $like_article += $number;

    }


}
