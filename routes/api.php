<?php

use App\Http\Controllers\ArticleController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/
Route::group(['prefix'=>'v1'], function () {

    Route::group(['prefix' => 'auth'], function ($router) {
        Route::post('create', 'AuthController@create');
        Route::post('login', 'AuthController@login');
        Route::post('logout', 'AuthController@logout');
        Route::post('refresh', 'AuthController@refresh');
        Route::post('me', 'AuthController@me');



    Route::GET('articles/{article}/hashtags',[ArticleController::class, 'viewHashtagsPost'])->name('View.hashtags.in.a.post');
   Route::GET('articles/{article}/hashtags/{hashtag}/create',[ArticleController::class, 'createHashtagPost'])->name('Create.hashtags.in.the.post');
    Route::GET('Articles/search',[ArticleController::class, 'searchPostsBasedHashtags'])->name('Ability to search for posts based on hashtags');
    Route::GET('hashtags/{hashtag}/delete',[ArticleController::class, 'removeArticleSpecificHashtag'])->name('Ability to delete posts based on specific hashtags');
    Route::GET('hashtags/search',[ArticleController::class, 'viewTrendingHashtagsLast24Hour'])->name('Ability to search for posts based on hashtags');
    Route::GET('Users/search',[UserController::class, 'searchUsername'])->name('search user to name user');
    Route::GET('users/{user}/viewPosts',[UserController::class, 'viewAccountAndPostsAccount'])->name('View the account and the posts(search) related to that account');
    Route::GET('users/{user}/delete',[UserController::class, 'deleteUser'])->name('delete User');
    Route::GET('users/{user}/comments/{comment}/likeComments',[UserController::class, 'likeComment'])->name('Ability to like comments');
    Route::GET('users/{user}/articles/{article}/likeArticle',[ArticleController::class, 'likeArticle'])->name('Ability to like comments');
    Route::post('Users/fallowUser',[UserController::class, 'fallowUser'])->name('fallow User');
    Route::GET('articles/{article}/viewComment',[ArticleController::class, 'viewCommentsPost'])->name('View comments on each post');
    Route::GET('articles/{article}/viewPhotosPost',[ArticleController::class, 'viewPhotosOnePost'])->name('view photos of one post');
    Route::GET('articles/{article}/viewPhotosAllPost',[ArticleController::class, 'viewPhotosAllPosts'])->name('View photos of all posts');
    Route::GET('articles/{article}/removeArticle',[ArticleController::class, 'removeArticle'])->name('remove Article');
    Route::Post('users/{user}/edit',[UserController::class, 'editUser'])->name('edit user');
    Route::GET('articles',[ArticleController::class, 'showSearchBasedOnTheMostLikes'])->name('Show search based on the most likes');
    Route::post('articles/create',[ArticleController::class, 'CreateArticle'])->name('Create Article');
    Route::post('articles/{article}/edit',[ArticleController::class, 'editArticlePhoto'])->name('edit Article Photo');
    Route::GET('Articles/showAll',[ArticleController::class, 'showAllArticle'])->name('show All Article');
    Route::post('Users/leaveComments',[UserController::class, 'leaveCommentsWithInputUserIdAndPost'])->name('Ability to leave comments with user ID and post input');
    Route::post('Users/create',[UserController::class, 'createUser'])->name('create User');
});
});
/*Route::group(['prefix' => 'v1'], function () {
    Route::resource('Users', 'UserController');
    Route::resource('Fallow', 'FallowController');
    Route::resource('Articles', 'ArticleController');
    Route::resource('Comment', 'CommentController');

});*/




